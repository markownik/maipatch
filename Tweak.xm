// Always make sure you clean up after yourself; Not doing so could have grave consequences!
#import <CommonCrypto/CommonDigest.h>
#import <Security/Security.h>
#import <UIKit/UIKit.h>
#import <objc/runtime.h>
#import "headers/CustomHeader.h"
#import "headers/AMUserDefaults.h"
#import "headers/KeychainItemWrapper.h"
#import "headers/LocalizationSystem.h"
#import "headers/CopyableLabel.h"
#import "headers/AppSystem.h"
#import "headers/CustomSettingsViewController.h"
#import "headers/iAMSettings.h"

@implementation NSString (MD5)
- (NSString *)MD5String {
	const char *cstr = [self UTF8String];
	unsigned char result[16];
	CC_MD5(cstr, strlen(cstr), result);

	return [NSString stringWithFormat:
		@"%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X",
		result[0], result[1], result[2], result[3],
		result[4], result[5], result[6], result[7],
		result[8], result[9], result[10], result[11],
		result[12], result[13], result[14], result[15]
	];
}
@end

@implementation NSString (NRStringFormatting)
- (NSString *)stringByFormattingAsCreditCardNumber
{
    NSMutableString *result = [NSMutableString string];
    __block NSInteger count = -1;
    [self enumerateSubstringsInRange:(NSRange){0, [self length]}
                             options:NSStringEnumerationByComposedCharacterSequences
                          usingBlock:^(NSString *substring, NSRange substringRange, NSRange enclosingRange, BOOL *stop) {
                              if ([substring rangeOfCharacterFromSet:[NSCharacterSet whitespaceCharacterSet]].location != NSNotFound)
                                  return;
                              count += 1;
                              if (count == 4) {
                                  [result appendString:@" "];
                                  count = 0;
                              }
                              [result appendString:substring];
                          }];
    return result;
}
@end

%hook AppSystem
- (id)getUDID
{
	NSString *uuid = @"";
	bool fresh = false;
	NSFileManager *fileManager = [NSFileManager defaultManager];

	NSArray* possibleURLs = [fileManager URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask];
	NSURL* appSupportDir = nil;
	if ([possibleURLs count] >= 1) {
		appSupportDir = [possibleURLs objectAtIndex:0];
	}
	NSString *path = [NSString stringWithFormat:@"%@", [[appSupportDir URLByAppendingPathComponent:@"UDID"] path]];

	if ([fileManager fileExistsAtPath:path] == YES) {
		NSDictionary* fileAttribs = [fileManager attributesOfItemAtPath:path error:nil];
		NSDate *modification = [fileAttribs objectForKey:NSFileModificationDate];
		NSDate *today = [NSDate date];
		NSTimeInterval secondsBetween = [today timeIntervalSinceDate:modification];
		int days = secondsBetween / 86400;
		if (days >= 5) {
			fresh = true;
		} else {
			NSData *data = [fileManager contentsAtPath:path];
			uuid = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
		}
	} else {
		fresh = true;
	}
	if (fresh) {
		uuid = [[[NSUUID UUID] UUIDString] MD5String];
		[uuid writeToFile:path atomically:YES encoding:NSUTF8StringEncoding error:NULL];
	}
	NSString *newUDID = MSHookIvar<NSString *>(self, "keyUDID");
	newUDID = uuid;
	return uuid;
}
- (id)init
{
	//LicAM5Time5AM
	//LicAM5Key5AM
	//Registration5EU
	//Registration5
	KeychainItemWrapper *keychainItem = [[KeychainItemWrapper alloc] initWithIdentifier:@"Registration5" accessGroup:nil];
	[keychainItem resetKeychainItem];
	[keychainItem release];
	keychainItem = [[KeychainItemWrapper alloc] initWithIdentifier:@"Registration5EU" accessGroup:nil];
	[keychainItem resetKeychainItem];
	[keychainItem release];
	keychainItem = [[KeychainItemWrapper alloc] initWithIdentifier:@"Registration5PL" accessGroup:nil];
	[keychainItem resetKeychainItem];
	[keychainItem release];

	//move all resources to mod directory
	// NSArray* possibleURLs = [[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask];
	// NSURL* appDocsDir = nil;
	// NSString *no = @"NO";
	// NSString *yes = @"YES";
	// if ([possibleURLs count] >= 1) {
	// 	appDocsDir = [possibleURLs objectAtIndex:0];
	// }
	// NSString *spoofdir = [NSString stringWithFormat:@"%@", [[appDocsDir URLByAppendingPathComponent:@"spoof"] path]];

	// [no writeToFile:spoofdir atomically:YES encoding:NSUTF8StringEncoding error:NULL];
	// NSString *oldDirectory = [[NSBundle mainBundle] resourcePath];
	// [yes writeToFile:spoofdir atomically:YES encoding:NSUTF8StringEncoding error:NULL];
	// NSString *newDirectory = [[NSBundle mainBundle] resourcePath];

	// NSArray *files = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:oldDirectory error:NULL];
	// if (![[NSFileManager defaultManager] fileExistsAtPath:newDirectory])
	// {
	// 	[[NSFileManager defaultManager] createDirectoryAtPath:newDirectory withIntermediateDirectories:NO attributes:nil error:NULL];
	// 	for (NSString *file in files) {
	// 		if(![[NSFileManager defaultManager] fileExistsAtPath:[newDirectory stringByAppendingPathComponent:file]])
	// 		{
	// 			[[NSFileManager defaultManager] copyItemAtPath:[oldDirectory stringByAppendingPathComponent:file]
	// 											toPath:[newDirectory stringByAppendingPathComponent:file]
	// 											error:NULL];
	// 		}
	// 	}
	// }
	return %orig;
}
- (id)getLicenceType
{
	id r = %orig;
	r = @"EU";
	return r;
}
- (id)getLicence
{
	[[objc_getClass("AMUserDefaults") standardUserDefaults] setObject:[NSString stringWithFormat:@"%@@automapa.pl", [self getUDID]] forKey:@"userMail"];
	return %orig;
}
- (_Bool)isMapLocked
{
	_Bool locked = MSHookIvar<_Bool>(self, "bLocked");
	locked = 0;
	_Bool r = %orig;
	r = 0;
	return r;
}
- (_Bool)trialExpired {
	_Bool r = %orig;
	r = 0;
	return r;
}
- (void)resetLicenceIfFirstRun {}
- (void)firstWarningSetShowed:(_Bool)arg1 {
	%orig(FALSE);
}
- (_Bool)isFirstRun {
	_Bool r = %orig;
	r = FALSE;
	return r;
}
- (void)nearExpirationInfo {
	[[objc_getClass("AMUserDefaults") standardUserDefaults] removeObjectForKey:@"7daysInfo"];
	[[objc_getClass("AMUserDefaults") standardUserDefaults] removeObjectForKey:@"3daysInfo"];
	[[objc_getClass("AMUserDefaults") standardUserDefaults] removeObjectForKey:@"1dayInfo"];
	%orig;
}
- (id)getExpirationDate {
	id r = %orig;
	NSString *str =@"2137-04-20";
	NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
	[formatter setDateFormat:@"yyyy-MM-dd"];
	[formatter setTimeZone:[NSTimeZone systemTimeZone]];
	NSDate *date = [formatter dateFromString:str];
	r = date;
	return r;
}

%end

%hook LicenceViewController
- (_Bool)validLicenceStart:(id)arg1
{
	%log;
	_Bool r = %orig(arg1);
	r = TRUE;
	return r;
}
- (void)viewDidLoad {
	%orig;
	UIButton *purchaseButton = MSHookIvar<UIButton *>(self, "purchaseButton");
	purchaseButton.hidden = YES;
	UISwitch *havingSwitch = MSHookIvar<UISwitch *>(self, "havingSwitch");
	havingSwitch.hidden = YES;
}
- (void)viewWillAppear:(_Bool)arg1 {
	%orig(arg1);
	UILabel *label = MSHookIvar<UILabel *>(self, "statusLabel");
	label.text = [NSString stringWithFormat:@"∞"];
	NSString *title = @"";
	NSString *msg = @"Thx mr. mark0wnik!";
	NSString *bttn = @"OK";
	AMAlertView *popup = [[objc_getClass("AMAlertView") alloc] initWithTitle:title message:msg delegate:nil cancelButtonTitle:bttn otherButtonTitles:nil];
	[popup show];
	[popup release];
}
- (id)tableView:(id)arg1 cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	UITableViewCell *cell;
	cell = %orig(arg1,indexPath);
	if (indexPath.section == 1)
	{
		cell.textLabel.text = @"I am a poorfag ( ͡° ͜ʖ ͡°)";
	}
	return cell;
}
%end

%hook InAppPurchaseViewController
- (_Bool)textFieldShouldReturn:(id)arg1
{
	return 0;
}
%new
- (_Bool)textFieldShouldBeginEditing:(id)arg1
{
	return 0;
}
%end

%hook UITextField
- (void)setText:(id)string
{
	if ([string rangeOfString:@"@automapa.pl"].location != NSNotFound) {
		string = @"oem@automapa.pl";
	}
	%orig(string);
}
%end

%hook CopyableLabel
- (void)touchEvent
{
	NSString *uuid = [[[NSUUID UUID] UUIDString] MD5String];
	NSFileManager *fileManager = [NSFileManager defaultManager];
	NSArray* possibleURLs = [fileManager URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask];
	NSURL* appSupportDir = nil;
	if ([possibleURLs count] >= 1) {
		appSupportDir = [possibleURLs objectAtIndex:0];
	}
	NSString *path = [NSString stringWithFormat:@"%@", [[appSupportDir URLByAppendingPathComponent:@"UDID"] path]];
	[uuid writeToFile:path atomically:YES encoding:NSUTF8StringEncoding error:NULL];
	[self setText:[uuid stringByFormattingAsCreditCardNumber]];
	[self setAdjustsFontSizeToFitWidth:YES];
	[[objc_getClass("AMUserDefaults") standardUserDefaults] setObject:[NSString stringWithFormat:@"%@@automapa.pl", uuid] forKey:@"userMail"];
}
%end

%hook AMAlertView
- (id)initWithTitle:(id)title message:(id)msg delegate:(id)arg3 cancelButtonTitle:(id)arg4 otherButtonTitles:(id)arg5 {
	%log;
	NSString *info = (NSString*)[[objc_getClass("LocalizationSystem") sharedLocalSystem] localizedStringForKey:@"loc_try_it_info" value:@""];
	if (([msg rangeOfString:info].location != NSNotFound))
	{
	 	return nil;
	}
	else
	{
		return %orig(title,msg,arg3,arg4,arg5);
	}
}
%end

/*

@interface Blowfisher : NSObject
{
}

- (id)decode:(id)arg1;
- (id)encode:(id)arg1;

@end

@interface KeychainItemWrapper : NSObject
{
    NSMutableDictionary *keychainItemData;
    NSMutableDictionary *genericPasswordQuery;
}

@property(retain, nonatomic) NSMutableDictionary *genericPasswordQuery; // @synthesize genericPasswordQuery;
@property(retain, nonatomic) NSMutableDictionary *keychainItemData; // @synthesize keychainItemData;
- (void)writeToKeychain;
- (id)secItemFormatToDictionary:(id)arg1;
- (id)dictionaryToSecItemFormat:(id)arg1;
- (void)resetKeychainItem;
- (id)objectForKey:(id)arg1;
- (void)setObject:(id)arg1 forKey:(id)arg2;
- (void)dealloc;
- (id)initWithIdentifier:(id)arg1 accessGroup:(id)arg2;

@end

*/

%hook KeychainItemWrapper
- (id)initWithIdentifier:(NSString *)arg1 accessGroup:(id)arg2
{
	%log;
	//HBLogDebug(@" - %@, %@", arg1, arg2);
	return %orig(arg1,arg2);
}
- (void)resetKeychainItem
{
	%log;
	%orig;
}
- (id)objectForKey:(NSString *)arg1
{
	%log;
	id r = %orig(arg1);
	HBLogDebug(@" - %@",r);
	return r;
}
- (void)setObject:(id)arg1 forKey:(NSString *)arg2
{
	%log;
	%orig(arg1, arg2);
}
%end

// %hook NSBundle
// - (NSString *)pathForResource:(NSString *)name ofType:(NSString *)ext
// {
// 	// %log;
// 	NSArray* possibleURLs = [[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask];
// 	NSURL* appDocsDir = nil;
// 	NSString *no = @"NO";
// 	NSString *yes = @"YES";
// 	if ([possibleURLs count] >= 1) {
// 		appDocsDir = [possibleURLs objectAtIndex:0];
// 	}
// 	NSString *spoofdir = [NSString stringWithFormat:@"%@", [[appDocsDir URLByAppendingPathComponent:@"spoof"] path]];

// 	[no writeToFile:spoofdir atomically:YES encoding:NSUTF8StringEncoding error:NULL];
// 	NSString *oldDirectory = [[NSBundle mainBundle] resourcePath];
// 	[yes writeToFile:spoofdir atomically:YES encoding:NSUTF8StringEncoding error:NULL];
// 	NSString *newDirectory = [[NSBundle mainBundle] resourcePath];

// 	NSString *p = %orig(name,ext);
// 	p = [p stringByReplacingOccurrencesOfString:oldDirectory
// 			withString:newDirectory];//[NSString stringWithFormat:@"%@/%@.%@", [self resourcePath], name, ext];//%orig(name,ext);
// 	//HBLogDebug(@"%@",p);
// 	if([name isEqualToString:@"SettingsMessages"] && [ext isEqualToString:@"plist"])
// 	{
// 		NSString *fixed_path = p;
// 		NSMutableDictionary *rootDict = [[NSMutableDictionary alloc] initWithContentsOfFile:fixed_path];
// 		NSMutableArray *preferences = [rootDict objectForKey:@"PreferenceSpecifiers"];
// 		NSMutableDictionary *clickSound = [NSMutableDictionary dictionaryWithObjectsAndKeys:
// 									@"Switch", @"Type",
// 									@"Click sound", @"Title",
// 									@"ClickSound", @"Key",
// 									[NSNumber numberWithBool:YES], @"DefaultValue",
// 									nil
// 									];
// 		if(![[[preferences objectAtIndex:15] objectForKey:@"Key"] isEqualToString:@"ClickSound"])
// 		{
// 			[preferences insertObject:clickSound atIndex:15];
// 			[rootDict writeToFile:fixed_path atomically:YES];
// 		}
// 	}

// 	if([name isEqualToString:@"SettingsInterface"] && [ext isEqualToString:@"plist"])
// 	{
// 		NSString *fixed_path = p;
// 		NSMutableDictionary *rootDict = [[NSMutableDictionary alloc] initWithContentsOfFile:fixed_path];
// 		NSMutableArray *preferences = [rootDict objectForKey:@"PreferenceSpecifiers"];
// 		NSMutableDictionary *clickSound = [NSMutableDictionary dictionaryWithObjectsAndKeys:
// 									@"Switch", @"Type",
// 									@"Map fullscreen", @"Title",
// 									@"MapFullscreen", @"Key",
// 									[NSNumber numberWithBool:YES], @"DefaultValue",
// 									nil
// 									];
// 		if(![[[preferences objectAtIndex:5] objectForKey:@"Key"] isEqualToString:@"MapFullscreen"])
// 		{
// 			[preferences insertObject:clickSound atIndex:5];
// 			[rootDict writeToFile:fixed_path atomically:YES];
// 		}
// 	}

// 	return p;
// }
// - (NSString *)resourcePath
// {
// 	NSArray* possibleURLs = [[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask];
// 	NSURL* appDocsDir = nil;
// 	if ([possibleURLs count] >= 1) {
// 		appDocsDir = [possibleURLs objectAtIndex:0];
// 	}
// 	NSString *spoofdir = [NSString stringWithFormat:@"%@", [[appDocsDir URLByAppendingPathComponent:@"spoof"] path]];
// 	NSData *data = [[NSFileManager defaultManager] contentsAtPath:spoofdir];
// 	NSString *spoof = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];

// 	if([spoof isEqualToString:@"NO"])
// 	{
// 		return %orig;
// 	}
// 	else
// 	{
// 		NSString *newDirectory = [NSString stringWithFormat:@"%@", [[appDocsDir URLByAppendingPathComponent:@"Resources"] path]];
// 		return newDirectory;
// 	}
// }
// %end

// %hook iSettings
// - (void)playClickSound
// {
// 	iAMSettings *amSettings = MSHookIvar<iAMSettings *>(self, "amSettings");
// 	if([amSettings getValueBool:@"SettingsMessages::ClickSound"])
// 	{
// 		%orig;
// 	}
// }
// %end

// %hook MapViewController
// - (BOOL)prefersStatusBarHidden
// {
// 	NSArray* possibleURLs = [[NSFileManager defaultManager] URLsForDirectory:NSLibraryDirectory inDomains:NSUserDomainMask];
// 	NSURL* appDocsDir = nil;
// 	if ([possibleURLs count] >= 1) {
// 		appDocsDir = [possibleURLs objectAtIndex:0];
// 	}
// 	NSString *prefdir = [NSString stringWithFormat:@"%@", [[appDocsDir URLByAppendingPathComponent:@"SettingsInterface.plist"] path]];
// 	NSMutableDictionary *rootDict = [[NSMutableDictionary alloc] initWithContentsOfFile:prefdir];
// 	NSMutableArray *preferences = [rootDict objectForKey:@"PreferenceSpecifiers"];
// 	if([[[preferences objectAtIndex:5] objectForKey:@"DefaultValue"] intValue])
// 	{
// 		return TRUE;
// 	}
// 	else
// 	{
// 		return FALSE;
// 	}
// }
// %end
