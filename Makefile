export THEOS = /opt/theos
#IP = ping -q -c 1 -t 1 markowniks.local | grep PING | sed -e "s/).*//" | sed -e "s/.*(//"
export THEOS_DEVICE_IP = markowniks.home#$IP #192.168.128.34 #192.168.2.13 #172.20.10.1 #192.168.128.31
SDKVERSION = 11.2
DEBUG = 1
ARCHS = arm64 #armv7 armv7s
TARGET = iphone:clang:latest:7.0

include $(THEOS)/makefiles/common.mk

TWEAK_NAME = MAiPatch
MAiPatch_FILES = Tweak.xm KeychainItemWrapper.m

include $(THEOS_MAKE_PATH)/tweak.mk
