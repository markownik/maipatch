@interface AMUserDefaults : NSObject
{
    NSMutableDictionary *dictionary;
}

+ (void)copyFromNSUserDefaults;
+ (void)initWithNSUserDefaults;
+ (id)path;
+ (id)standardUserDefaults;
- (void)removeAll;
- (void)registerDefaults:(id)arg1;
- (void)synchronize;
- (void)setDouble:(double)arg1 forKey:(id)arg2;
- (void)setFloat:(float)arg1 forKey:(id)arg2;
- (void)setInteger:(long long)arg1 forKey:(id)arg2;
- (void)setBool:(_Bool)arg1 forKey:(id)arg2;
- (long long)integerForKey:(id)arg1;
- (id)stringArrayForKey:(id)arg1;
- (id)dataForKey:(id)arg1;
- (id)dictionaryForKey:(id)arg1;
- (id)arrayForKey:(id)arg1;
- (_Bool)boolForKey:(id)arg1;
- (id)stringForKey:(id)arg1;
- (double)doubleForKey:(id)arg1;
- (float)floatForKey:(id)arg1;
- (void)removeObjectForKey:(id)arg1;
- (id)objectForKey:(id)arg1;
- (void)setObject:(id)arg1 forKey:(id)arg2;
- (void)dealloc;
- (id)init;

@end
