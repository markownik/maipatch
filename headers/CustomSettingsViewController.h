#import <UIKit/UIKit.h>
#import "UIBaseTableViewController.h"
@interface CustomSettingsViewController : UIBaseTableViewController
{
    NSString *viewKey;
    NSMutableString *title;
    NSMutableArray *sections;
    UIImage *tabBarImage;
    UIResponder *responder;
}
- (void)scrollViewWillBeginDragging:(id)arg1;
- (void)dealloc;
- (void)viewDidUnload;
- (void)didReceiveMemoryWarning;
- (void)viewWillAppear:(_Bool)arg1;
- (void)loadView;
- (void)tableView:(id)arg1 didSelectRowAtIndexPath:(id)arg2;
- (void)deselect;
- (id)initWithViewKey:(id)arg1;
- (id)init;
- (id)baseinit;

@end
