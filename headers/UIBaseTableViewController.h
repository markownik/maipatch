#import <UIKit/UIKit.h>
@interface UIBaseTableViewController : UITableViewController
{
    _Bool bViewIsPortrait;
    _Bool bViewIsLandscape;
}

- (void)didRotateFromInterfaceOrientation:(long long)arg1;
- (void)willRotateToInterfaceOrientation:(long long)arg1 duration:(double)arg2;
- (id)tableView:(id)arg1 titleForDeleteConfirmationButtonForRowAtIndexPath:(id)arg2;
- (_Bool)prefersStatusBarHidden;
- (unsigned long long)supportedInterfaceOrientations;
- (_Bool)shouldAutorotate;
- (struct CGRect)screenSize:(_Bool)arg1;
- (_Bool)shouldAutorotateToInterfaceOrientation:(long long)arg1;
- (void)viewWillAppear:(_Bool)arg1;
- (id)initWithStyle:(long long)arg1;
- (id)init;

@end
