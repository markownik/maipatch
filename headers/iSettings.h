#import "iAMSettings.h"

@interface iSettings : NSObject <UITabBarControllerDelegate>
{
    _Bool readyForSaving;
    NSMutableDictionary *theDictionary;
    NSMutableDictionary *theViews;
    NSMutableDictionary *theViewsStatic;
    iAMSettings *amSettings;
    NSString *documentsPath;
    MapViewController *mapController;
    UITabBarController *settingsTabBarController;
    UINavigationController *mapRouteController;
    UINavigationController *mapLicenceController;
    UITabBarController *mapPOI;
    UINavigationController *mapSearch;
    UINavigationController *mapPoint;
    id searchViewController;
    AddressTabViewController *searchTabController;
    NonAutoDisplayWindow *window;
    UIBarButtonItem *barButtonMap;
    NSMutableDictionary *hideControls;
    UIImage *imgRouteStart;
    UIImage *imgRouteVia;
    UIImage *imgRouteEnd;
    UIImage *imgCheckOff;
    UIImage *imgCheckOn;
    UIImage *imgCheckPart;
    Route *route;
    POIset *POIs;
    POIfavSet *POIfavss;
    AVAudioPlayer *clickSound;
    SummaryViewController *summaryViewController;
    POISearchViewController *poiSearchViewController;
    UIActivityIndicatorView *splashSpinnerView;
    AppInfoView *appInfo;
    RoadLoadTableController *roadLoadTableController;
    UIImageView *splashView;
    _Bool disableRotate;
    _Bool bModalDismissInvoked;
}

@property(retain, nonatomic) iAMSettings *amSettings; // @synthesize amSettings;

+ (id)instance;
- (void)dealloc;
- (void)updateAllPlists;
- (void)updatePlist:(id)arg1 withName:(id)arg2;
- (void)updateItemAtPath:(id)arg1 withPath:(id)arg2;
- (void)initSettingsViews;
- (void)tabBarController:(id)arg1 didSelectViewController:(id)arg2;
- (void)loadView:(id)arg1;
- (id)loadViewSettingsPlist:(id)arg1;
- (void)loadSettingsForPlist:(id)arg1;
- (id)devicePlistName:(id)arg1;
- (void)showMe:(id)arg1 forHideKey:(id)arg2;
- (void)hideMe:(id)arg1 forHideKey:(id)arg2;
- (void)loadAllViews;
- (void)releaseViews;
- (void)turnOverMapPoint;
- (void)turnOverMapPointBackWithPopToRoot;
- (void)turnOverMapPointBack;
- (void)turnOverMapSearch;
- (void)turnOverMapSearchBack;
- (void)turnOverMapPOI;
- (void)turnOverMapPOIBack;
- (void)turnOverMapLicence;
- (void)turnOverMapLicenceBack;
- (void)turnOverMapGPS;
- (void)turnOverMapGPSBack;
- (void)turnOverMapSettingsRoute;
- (void)turnOverMapSettingsRouteBackNoLoad;
- (void)turnOverMapSettingsRouteBack;
- (void)turnOverBack;
- (void)turnOver;
- (void)playClickSound;
- (void)turnFrom:(int)arg1 to:(int)arg2 back:(_Bool)arg3;
- (void)turnOverFrom:(int)arg1 to:(int)arg2 back:(_Bool)arg3;
- (id)getProperViewController:(int)arg1;
- (id)getControlWithKey:(id)arg1 fromView:(id)arg2;
- (id)getViewWithKey:(id)arg1;
- (id)getViewWithKey:(id)arg1 force:(_Bool)arg2;
- (id)getClickSound;
- (void)loadSearchTab;
- (id)init;
- (void)reportMemoryDefault;
- (void)reportMemory:(id)arg1;
- (void)dismissShowedModalView;
- (_Bool)dismissCondition;

@end
