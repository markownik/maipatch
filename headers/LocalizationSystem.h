@interface LocalizationSystem : NSObject
{
    NSString *language;
}

+ (id)alloc;
+ (id)sharedLocalSystem;
- (void)resetLocalization;
- (id)getLanguage;
- (void)setLanguage:(id)arg1;
- (id)localizedStringForKey:(id)arg1 value:(id)arg2;
- (id)init;

@end
