@interface NSString (MD5)
- (NSString *)MD5String;
@end

@interface NSString (NRStringFormatting)
- (NSString *)stringByFormattingAsCreditCardNumber;
@end
