@interface AMAlertView : UIView <UITextFieldDelegate>
{
    UIView *frameView;
    UIView *contentView;
    UILabel *titleLabel;
    UILabel *messageLabel;
    UILabel *dismissLabel;
    UIActivityIndicatorView *activity;
    NSMutableArray *buttons;
    NSMutableArray *textInputs;
    NSMutableArray *textInputsWithLabel;
    NSMutableArray *switchesWithLabel;
    NSMutableArray *progresses;
    NSMutableArray *textViews;
    NSMutableArray *datePickers;
    _Bool mini;
    id <UIAlertViewDelegate> alertViewDelegate;
    int clickedButton;
    int cancelButtonIndex;
    int tag;
    int dismissAfterSeconds;
    CDUnknownBlockType CloseBlock;
}

@property(nonatomic) int cancelButtonIndex; // @synthesize cancelButtonIndex;
@property(nonatomic) int tag; // @synthesize tag;
@property(nonatomic) int clickedButton; // @synthesize clickedButton;
@property(copy, nonatomic) CDUnknownBlockType CloseBlock; // @synthesize CloseBlock;
- (void)dismissAfter:(int)arg1;
- (void)updateDismissTime;
- (void)close;
- (void)show;
- (void)show:(_Bool)arg1;
- (void)afterAnimation;
- (void)beforeAnimation;
- (void)updateMessage:(id)arg1;
- (void)updateTitle:(id)arg1;
- (void)deviceOrientationDidChange:(id)arg1;
- (void)setupOrientation:(long long)arg1;
- (void)keyboardWillHideOrShow:(id)arg1;
- (_Bool)textFieldShouldReturn:(id)arg1;
- (id)getMessageLabel;
- (id)progress:(int)arg1;
- (id)textInputWithLabel:(int)arg1;
- (id)textInput:(int)arg1;
- (_Bool)valueForSwitchWithLabel:(int)arg1;
- (id)valueForTextInputWithLabel:(int)arg1;
- (id)valueForTextInput:(int)arg1;
- (void)addDatePicker:(id)arg1;
- (void)addTextViewWithText:(id)arg1;
- (void)addActivity;
- (void)addProgress;
- (void)addSwitch:(_Bool)arg1 WithLabel:(id)arg2;
- (id)hitTest:(struct CGPoint)arg1 withEvent:(id)arg2;
- (void)addSwitch:(_Bool)arg1 WithLabel:(id)arg2 small:(_Bool)arg3;
- (void)addTextInputWithLabel:(id)arg1;
- (void)addTextInput;
- (void)buttonHandler:(id)arg1;
- (void)addButtonWithNewLineAndTitle:(id)arg1;
- (void)addButtonWithTitle:(id)arg1;
- (void)layoutSubviews:(_Bool)arg1;
- (void)updateLabelHeight:(id)arg1;
- (struct CGSize)textSize:(id)arg1;
- (void)dismissWithClickedButtonIndex:(long long)arg1 animated:(_Bool)arg2;
- (id)initWithTitle:(id)arg1 message:(id)arg2 delegate:(id)arg3 cancelButtonTitle:(id)arg4 otherButtonTitles:(id)arg5;
- (void)dealloc;
- (void)relayout;
- (void)initOnMainThread;
- (id)init;

// Remaining properties
@property(readonly, copy) NSString *debugDescription;
@property(readonly, copy) NSString *description;
@property(readonly) unsigned long long hash;
@property(readonly) Class superclass;

@end

@interface AMAlertViewSynchronize : AMAlertView
{
    _Bool running;
}

- (void)close;
- (void)show;

@end
