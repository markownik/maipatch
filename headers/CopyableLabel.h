@interface CopyableLabel : UILabel
{
}

- (void)touchEvent;
- (void)copy:(id)arg1;
- (_Bool)becomeFirstResponder;
- (_Bool)canBecomeFirstResponder;
- (_Bool)canPerformAction:(SEL)arg1 withSender:(id)arg2;

@end
