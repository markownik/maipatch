@interface iAMSettings : NSObject
{
    NSString *pathToWrite;
    NSMutableDictionary *allSettings;
}

- (id)getValueText:(id)arg1;
- (float)getValueSelectedFloat:(id)arg1;
- (id)getValueSelectedVoice:(id)arg1;
- (int)getValueSelectedIndex:(id)arg1;
- (float)getValueFloat:(id)arg1;
- (_Bool)getValueBool:(id)arg1;
- (id)getDictFromAllWithKey:(id)arg1;
- (id)getDict:(id)arg1;
- (void)setValueNumber:(id)arg1 forKey:(id)arg2;
- (void)setValueText:(id)arg1 forKey:(id)arg2;
- (void)setValueSelectedVoice:(id)arg1 forKey:(id)arg2;
- (void)setValueSelectedIndex:(id)arg1 forKey:(id)arg2;
- (void)setValueBool:(_Bool)arg1 forKey:(id)arg2;
- (void)saveSettingsFor:(id)arg1;
- (void)clearAllSettings;
- (void)addViewSettings:(id)arg1 andKey:(id)arg2;
- (id)initWithWritePath:(id)arg1;

@end
